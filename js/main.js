$(document).ready(function(){
    $("#navbar").hide();
    var alturaPagina = $(window).height();
    var alturaScroll = $(window).scrollTop(); 

    $("#banner-principal").css({"height": + alturaPagina + "px"});
    
    $("#return").hide();
    $(window).scroll(function(){
        if($(this).scrollTop() > 10){
            $("#return").fadeIn();
        }
        
        else{
            $("#return").fadeOut();
        }
    });
    
    $("#return").on('click', function(){ 
          $("body,html").animate({
            scrollTop: 0
    },700);        
});

    $(window).scroll(function(){
        if($(this).scrollTop() >= alturaPagina){
            $("#navbar").fadeIn();
    }

    else{
        $("#navbar").fadeOut();
    }
    });

    $("#menu-content-wrap").hide();
    $(".menu-contents").hide();
    $(".image").hide();
    $(".title-image").hide();

    $("#menu-content-wrap").fadeIn(400, function(){
        $(".menu-contents").fadeIn(400, function(){
            $(".image").fadeIn(400, function(){
                $(".title-image").fadeIn();
            });
        });
    });

});